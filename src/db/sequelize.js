import { Sequelize } from 'sequelize'
import config from '../config'

const USER = encodeURIComponent(config.db.user)
const PASSWORD = encodeURIComponent(config.db.password)
const URI = `${config.db.dialect}://${USER}:${PASSWORD}@${config.db.server}:${config.db.port}/${config.db.dbName}`

const getSequelize = () => {
  try {
    const sequelize = new Sequelize(URI, {
      dialect: config.db.dialect,
      loggin: config.db.loggin
    })
    //await sequelize.authenticate()
    return sequelize
  } catch (error) {
    throw error
  }
}
export default getSequelize()

//setupModels(sequelize)
//sequelize.sync()
