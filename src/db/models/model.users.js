import { DataTypes } from 'sequelize'
import db from '../sequelize'

const Usuario = db.define(
  'user',
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    email: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING
    },
    createAt: {
      allowNull: false,
      type: DataTypes.DATE,
      field: 'create_at', // el verdadero nombre como quiero que se cree
      defaultValue: DataTypes.NOW
    }
  },
  {
    timestamps: false // Para que no se creen los timestamps: createdAt y updatedAt por default
  }
)

//Creamos la tabla
Usuario.sync()

export default Usuario
