'use strict'
import { UserSchema } from '../models/model.users'
module.exports = {
  up: async (queryInterface) => {
    await queryInterface.createTable('users', UserSchema)
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable('users')
  }
}
