import Coonect from './SequelizeConnect'

export default class ResourcesService {
  /**
   * @description DAO postgresDB tables
   * @param {string} table - table name
   */
  constructor(table) {
    this.table = table
    this.client = new Coonect()
  }

  /**
   * @description find all registers in table
   * @returns {array} - response query mongoDB as array
   */
  async findAll() {
    const db = await this.client.connect()
    await db.models.User.findAll()
    const [data] = await this.client.query(`SELECT * FROM ${this.table}`)
    return data
  }
}
