import { Sequelize as Client } from 'sequelize'
import config from '../../config'
import setupModels from '../../db/models'
export default class SequelizeConnect {
  /**
   * @private
   * @description singleton pattern for pool connection
   * @returns {object} - connection client
   */

  async connect() {
    try {
      if (!SequelizeConnect.connection) {
        const USER = encodeURIComponent(config.db.user)
        const PASSWORD = encodeURIComponent(config.db.password)
        const URI = `${config.db.dialect}://${USER}:${PASSWORD}@${config.db.server}:${config.db.port}/${config.db.dbName}`

        SequelizeConnect.connection = new Client(URI, {
          dialect: config.db.dialect,
          loggin: false
        })
        setupModels(SequelizeConnect.connection)
        SequelizeConnect.connection.sync()
      }
      return SequelizeConnect.connection
    } catch (error) {
      throw error
    }
  }
  /**
   * @description query process in table
   * @param {string} request - SQL string request
   * @returns {Object} - response query postgresDB
   */
  // async query() {
  //   try {
  //     const db = await this.connect()

  //     return await db.models.User.findAll()
  //   } catch (error) {
  //     throw error
  //   }
  // }
  // async query(request) {
  //   try {
  //     const db = await this.connect()

  //     return await db.query(request)
  //   } catch (error) {
  //     throw error
  //   }
  // }
}
