export function ErrorHandler(message, statusCode = 500, descError = 'typod') {
  const error = new Error(message)
  error.statusCode = statusCode
  error.descStatus = descError
  return error
}
