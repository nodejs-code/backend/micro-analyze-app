import faker from 'faker'
import boom from '@hapi/boom'

import Usuario from '../../../db/models/model.users'

class ProductsService {
  constructor(limit) {
    this.productos = []
    this.limit = limit
    this.generate()
    //this.pool = pool
  }
  generate() {
    const limit = this.limit || 1
    for (let index = 0; index < limit; index++) {
      this.productos.push({
        id: faker.datatype.uuid(),
        name: faker.commerce.productName(),
        price: Number(faker.commerce.price()),
        image: faker.image.imageUrl()
      })
    }
  }

  async create(data) {
    try {
      const newProduct = {
        id: faker.datatype.uuid(),
        ...data
      }
      await Usuario.create(newProduct)
      return newProduct
    } catch (error) {
      console.log('el error cusa:::', error)
      throw error
    }
  }

  async find() {
    try {
      const rta = await Usuario.findAll()
      return rta
    } catch (error) {
      throw error
    }
  }

  async findOne(id) {
    try {
      const user = await Usuario.findByPk(id)
      if (!user) {
        throw boom.notFound('Usuario no encontrado!!!')
      }
      return user
    } catch (error) {
      throw error
    }
  }

  async update(id, changes) {
    try {
      const user = await this.findOne(id)
      const rta = await user.update(changes)
      return rta
    } catch (error) {
      throw error
    }
  }

  async delete(id) {
    const user = await this.findOne(id)
    user.destroy()
    return { id }
  }
}

export default ProductsService
