import { Router } from 'express'
import upload from '../../../network/middlewares/multer'
import {
  validateCSV,
  listarProductos,
  listarProductosById,
  addProducto,
  updateProducto,
  deleteProducto
} from './controller'

import validatorHandler from '../../../network/middlewares/validator.schema'
import {
  createProductSchema,
  getProductSchema,
  updateProductSchema
} from '../../../network/schemas/productoSchema'
const router = Router()

router.post('/upload', upload.single('file'), validateCSV)
router.get('/', listarProductos)
router.post('/', validatorHandler(createProductSchema, 'body'), addProducto)
router.get('/:id', validatorHandler(getProductSchema, 'params'), listarProductosById)
router.patch(
  '/:id',
  validatorHandler(getProductSchema, 'params'),
  validatorHandler(updateProductSchema, 'body'),
  updateProducto
)
router.delete('/:id', deleteProducto)

export default router
