import { success } from '../../../network/response'
import ServiceFile from '../../../utils/servicesFiles'
import ServicesCsv from '../../../utils/cvs'
import ServicesDiccionaryValidate from '../../../utils/diccionaryValidate'
import ProductsService from './Services'

const fs = new ServiceFile('public/uploads')
const csv = new ServicesCsv('public/uploads')
const productoService = new ProductsService(100)

export async function validateCSV(req, res, next) {
  try {
    
    const fileEntidad = req.file
    const { diccionario } = req.body
    const csvtojson = await csv.csvtojson({ file: fileEntidad.filename })
    await fs.deleteFile({ file: req.file.filename })
    const dicc = new ServicesDiccionaryValidate(csvtojson, diccionario)
    await dicc.getValidTotalColumnas()
    await dicc.getValidNamesColumns()
    await dicc.getValidDatatype()
    success(req, res, { fileEntidad })
  } catch (error) {
    next(error)
  }
}

export async function listarProductos(req, res, next) {
  try {
    const listaProductos = await productoService.find()
    success(req, res, listaProductos)
  } catch (error) {
    next(error)
  }
}

export async function listarProductosById(req, res, next) {
  try {
    const { id } = req.params
    const producto = await productoService.findOne(id)
    success(req, res, producto)
  } catch (error) {
    next(error)
  }
}

export async function addProducto(req, res, next) {
  try {
    const dataProducto = req.body
    const producto = await productoService.create(dataProducto)
    success(req, res, producto, 201)
  } catch (error) {
    next(error)
  }
}

export async function updateProducto(req, res, next) {
  try {
    const { id } = req.params
    const dataProductoChange = req.body
    const productoUpdated = await productoService.update(id, dataProductoChange)
    success(req, res, productoUpdated, 200)
  } catch (error) {
    next(error)
  }
}

export async function deleteProducto(req, res, next) {
  try {
    const { id } = req.params
    const message = await productoService.delete(id)
    success(req, res, message, 200)
  } catch (error) {
    next(error)
  }
}
