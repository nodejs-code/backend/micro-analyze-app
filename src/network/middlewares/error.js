import { error } from '../response'
import { ValidationError } from 'sequelize'
export function logError(err, req, res, next) {
  console.log(err)
  next(err)
}

export function erroHandler(err, req, res, next) {
  const msnError = {
    statusCode: err.statusCode || 500,
    error: err.descStatus,
    message: err.message,
    stack: err.stack
  }
  error(req, res, msnError, 500)
  next()
}

export function ormErrorHandler(err, req, res, next) {
  if (err instanceof ValidationError) {
    const msnError = {
      statusCode: 409,
      error: err.name,
      message: err.errors
    }
    error(req, res, msnError, 409)
  } else {
    next(err)
  }
}

export function boomErroHandler(err, req, res, next) {
  if (err.isBoom) {
    const {
      output: { statusCode, payload }
    } = err
    error(req, res, payload, statusCode)
  } else {
    next(err)
  }
}
