const Joi = require('joi')

const id = Joi.string()
// const name = Joi.string().min(3).max(15)
// const price = Joi.number().integer().min(10)
// const image = Joi.string().uri()
const email = Joi.string().email()
const password = Joi.string()
export const createProductSchema = Joi.object({
  //name: name.required(),
  //price: price.required(),
  //image: image.required()
  email: email.required(),
  password: password.required()
})

export const updateProductSchema = Joi.object({
  // name: name,
  // price: price,
  // image: image
  email: email.required(),
  password: password
})

export const getProductSchema = Joi.object({
  id: id.required()
})
