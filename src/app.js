import express from 'express'
import routers from './network/routing'
import {
  logError,
  boomErroHandler,
  erroHandler,
  ormErrorHandler
} from './network/middlewares/error'
import config from './config'
import cors from 'cors'
const app = express()

//setting
app.set('port', config.port)

//middlewares
app.use(cors())
app.use(express.static('public'))
app.use('/static', express.static(__dirname + 'public'))
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

//routers
routers(app)

//midleware error
app.use(logError)
app.use(ormErrorHandler)
app.use(boomErroHandler)
app.use(erroHandler)

export default app
